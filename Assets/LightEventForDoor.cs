﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightEventForDoor : MonoBehaviour 
{
    public Material hitLightMaterial;

    int id = 0;
    bool isDetected = false;
    Color c = Color.black;

    void Start()
    {
        id = gameObject.GetInstanceID();

        Light2D.RegisterEventListener(LightEventListenerType.OnStay, OnLightStay);
        Light2D.RegisterEventListener(LightEventListenerType.OnEnter, OnLightEnter);
        Light2D.RegisterEventListener(LightEventListenerType.OnExit, OnLightExit);
    }

    void OnDestroy()
    {
        /* (!) Make sure you unregister your events on destroy. If you do not
         * you might get strange errors (!) */

        Light2D.UnregisterEventListener(LightEventListenerType.OnStay, OnLightStay);
        Light2D.UnregisterEventListener(LightEventListenerType.OnEnter, OnLightEnter);
        Light2D.UnregisterEventListener(LightEventListenerType.OnExit, OnLightExit);
    }

    void Update()
    {
        if (isDetected)
            transform.parent.GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(transform.parent.GetComponentInChildren<MeshRenderer>().material.color, c, Time.deltaTime * 10f);
        else
            transform.parent.GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(transform.parent.GetComponentInChildren<MeshRenderer>().material.color, Color.black, Time.deltaTime * 5f);

        if (GameManager.Instance.isBurning)
        {
            transform.parent.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
        }
        isDetected = false;
    }

    void OnLightEnter(Light2D l, GameObject g)
    {
        if (g.GetInstanceID() == id)
        {
            c += l.LightColor;
        }
    }

    void OnLightStay(Light2D l, GameObject g)
    {
        if (g.GetInstanceID() == id)
        {
            isDetected = true;
        }
    }

    void OnLightExit(Light2D l, GameObject g)
    {
        if (g.GetInstanceID() == id)
        {
            c -= l.LightColor;
        }
    }
}
