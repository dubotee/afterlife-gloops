﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class InteractionReceiver : MonoBehaviour {
    private LineRenderer lineRenderer;
    private List<Vector3> gizmos;	
    List<Vector3> pointsList = new List<Vector3>();
    private BoxCollider boxCollider;
	// Use this for initialization
    bool isStart = false;
    float colorAlpha = 1;
    // Structure for line points
    struct Line
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };

	public void Load()
	{
		boxCollider = GetComponent<BoxCollider>();
		scaleCollider();
		lineRenderer = GetComponent<LineRenderer>();
		gizmos = new List<Vector3>();
    }
    
	public void EnableInteraction(bool enabled)
	{
		if (boxCollider != null)
			boxCollider.enabled = enabled;
	}

    // Update is called once per frame
	void Update () {
        if (isStart == false)
        {
            fadeOutLine();
        }
	}

    void OnMouseDown()
    {
		if (GameManager.Instance.CanMove() == true)
        {
            lineRenderer.SetVertexCount(0);
            pointsList.RemoveRange(0, pointsList.Count);
            isStart = true;
            colorAlpha = 1;
            lineRenderer.SetColors(Color.white, Color.white);
        }
        //renderLineSegments();
    }

    void OnMouseDrag()
    {
        if (GameManager.Instance.CanMove() == true)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            if (!pointsList.Contains(mousePos))
            {
                pointsList.Add(mousePos);
                lineRenderer.SetVertexCount(pointsList.Count);
                lineRenderer.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
            }
        }
    }

    void OnMouseUp()
    {
		if (GameManager.Instance.CanMove() == true)
        {
            isStart = false;
            bezierReduce();
            GameManager.Instance.StartMoveCharacter(this.pointsList);
        }
    }

    void scaleCollider()
    {
        boxCollider.size = new Vector3(20, GameManager.Instance.Background.TileRowCount * 10);
        boxCollider.center = new Vector3(0, (GameManager.Instance.Background.TileRowCount * 5) - 5);
    }

    void fadeOutLine()
    {
        Color start = Color.white;
        start.a = colorAlpha;
        Color end = Color.white;
        end.a = colorAlpha;
        if (colorAlpha > 0)
            colorAlpha -= Time.deltaTime * 0.5f;
        else
            colorAlpha = 0;

        lineRenderer.SetColors(start, end);
    }

    public void ClearPath()
    {
        lineRenderer.SetVertexCount(0);
    }

    private bool isLineCollide()
    {
        if (pointsList.Count < 2)
            return false;
        int TotalLines = pointsList.Count - 1;
        Line[] lines = new Line[TotalLines];
        if (TotalLines > 1)
        {
            for (int i = 0; i < TotalLines; i++)
            {
                lines[i].StartPoint = (Vector3)pointsList[i];
                lines[i].EndPoint = (Vector3)pointsList[i + 1];
            }
        }
        for (int i = 0; i < TotalLines - 1; i++)
        {
            Line currentLine;
            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (isLinesIntersect(lines[i], currentLine))
                return true;
        }
        return false;
    }

    private bool checkPoints(Vector3 pointA, Vector3 pointB)
    {
        return (pointA.x == pointB.x && pointA.y == pointB.y);
    }

    private bool isLinesIntersect(Line L1, Line L2)
    {
        if (checkPoints(L1.StartPoint, L2.StartPoint) ||
            checkPoints(L1.StartPoint, L2.EndPoint) ||
            checkPoints(L1.EndPoint, L2.StartPoint) ||
            checkPoints(L1.EndPoint, L2.EndPoint))
            return false;

        return ((Mathf.Max(L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min(L2.StartPoint.x, L2.EndPoint.x)) &&
               (Mathf.Max(L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min(L1.StartPoint.x, L1.EndPoint.x)) &&
               (Mathf.Max(L1.StartPoint.y, L1.EndPoint.y) >= Mathf.Min(L2.StartPoint.y, L2.EndPoint.y)) &&
               (Mathf.Max(L2.StartPoint.y, L2.EndPoint.y) >= Mathf.Min(L1.StartPoint.y, L1.EndPoint.y))
               );
    }

    private void renderLineSegments()
    {
        gizmos = pointsList;
        //Debug.Log(gizmos.Count);
        SetLinePoints(pointsList);
    }

    private void bezierReduce()
    {
        BezierPath bezierPath = new BezierPath();
        //Debug.Log(pointsList.Count);
        bezierPath.Interpolate(pointsList, .1f);

        //List<Vector3> drawingPoints = bezierPath.GetDrawingPoints2();
        //Debug.Log(gizmos.Count);

        gizmos = bezierPath.GetControlPoints();
        //gizmos.Reverse();
        //SetLinePoints(drawingPoints);
    }

    private void SetLinePoints(List<Vector3> drawingPoints)
    {
        lineRenderer.SetVertexCount(drawingPoints.Count);

        for (int i = 0; i < drawingPoints.Count; i++)
        {
            lineRenderer.SetPosition(i, drawingPoints[i]);
        }
    }

    public void OnDrawGizmos()
    {
        if (pointsList == null)
        {
            return;
        }

        for (int i = 0; i < pointsList.Count; i++)
        {
            Gizmos.DrawWireSphere(pointsList[i], 1f);
        }
    }
}
