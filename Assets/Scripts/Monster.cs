﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Monster will follow the routine which GD has defined
/// Monster will have many ability
/// Monster will follow Character when he in view sight
/// </summary>

public class Monster : MonoBehaviour {
    public float Speed;
    iTweenPath path;
	// Use this for initialization
	void Start () {
        path = GetComponent<iTweenPath>();
		if (path == null)
		{
			path = transform.parent.GetComponent<iTweenPath>();
		}
        StartPatrol();
        GameManager.Instance.MonsterList.Add(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void StartPatrol()
    {
        iTween.MoveTo(gameObject,
            iTween.Hash("name", path.name,
                        "path", iTweenPath.GetPath(path.pathName),
                        "movetopath", false,
                        "orienttopath", true,
                        "speed", Speed,
                        "axis", "z",
                        "looptype", iTween.LoopType.pingPong,
                        "easetype", iTween.EaseType.linear));
    }

    public void StopPatrol()
    {
        iTween.StopByName(path.name);
    }

    public void PausePatrol()
    {
        iTween.Pause(gameObject);
    }

    public void ResumePatrol()
    {
        iTween.Resume(gameObject);
    }
}
