﻿using UnityEngine;
using System.Collections;

public class SceneManager : Singleton<SceneManager>
{
    public enum GameState
    {
        INTRO,
        MAIN_MENU,
        INGAME
    }

    GameState _state;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public GameState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            switch (_state)
            {
                case GameState.INTRO:
                    if (Application.loadedLevelName != "Intro") Application.LoadLevel("Intro");
                    break;
                case GameState.MAIN_MENU:
                    if (Application.loadedLevelName != "MainMenu") Application.LoadLevel("MainMenu");
                    break;
                case GameState.INGAME:
                    if (Application.loadedLevelName != "Ingame2") Application.LoadLevel("Ingame3");
                    break;
            }
        }
    }
}
