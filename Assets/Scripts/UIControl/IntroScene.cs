﻿using UnityEngine;
using System.Collections;

public class IntroScene : MonoBehaviour {

	private static bool s_wasIntro = false;

    public GameObject Buttons;
    public GameObject Texts;

    float timer;

	void Start()
	{
		if (s_wasIntro)
		{
			timer = 10;
		}
	}
	
	void Update ()
	{
        timer += Time.deltaTime;
        if (timer >= 8)
        {
            Texts.SetActive(false);
            Buttons.SetActive(true);
			s_wasIntro = true;
		}
	}

	public void OnButtonClick(string mapName)
	{
		GameManager.s_levelPath = mapName;
		SceneManager.Instance.State = SceneManager.GameState.INGAME;
	}
}
