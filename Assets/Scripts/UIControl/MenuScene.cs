﻿using UnityEngine;
using System.Collections;

public class MenuScene : MonoBehaviour {
    public GameObject Credit;
    public GameObject Menu;
    public GameObject Soul;

	public GameObject helpPanel;

	// Use this for initialization
	void Start () {
        Menu.SetActive(true);
        Credit.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Move to intro scene
    public void OnButtonSingleClick()
    {
        SceneManager.Instance.State = SceneManager.GameState.INTRO;
    }

    //Establish connection
    public void OnButtonMultiplayerClick()
    {

    }

    public void OnCreditClick()
    {
        Menu.SetActive(true);
        Credit.SetActive(false);
        Soul.SetActive(false);
    }

    //Show popup credit
    public void OnButtonCreditClick()
    {
        Credit.SetActive(true);
        Menu.SetActive(false);
        Soul.SetActive(true);
    }

	public void ShowHelp()
	{
		helpPanel.SetActive(true);
	}

	public void HideHelp()
	{
		helpPanel.SetActive(false);
	}
}
