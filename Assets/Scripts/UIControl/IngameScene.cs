﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class IngameScene : MonoBehaviour {
    public Image ResultPanel;
    public Text ResultText;
    public GameObject PanelGuide;
	public GameObject HelpPanel;

    public GameObject ImageWin;
    public GameObject ImageLose;

	public GameObject playPanel;
	public GameObject burnButton;
	public Text burnText;
	public Text moveText;
	public Text timeText;

    float time;

	void Start()
	{
		HelpPanel.SetActive(false);
	}

	public void ShowHint(Action callback)
	{
		StartCoroutine(_ShowHint(callback));
	}

	IEnumerator _ShowHint(Action callback)
	{
		playPanel.SetActive(false);
		PanelGuide.SetActive(true);

		int count = 3;
		while(count > 0)
		{
			yield return new WaitForSeconds(1);
			count--;
		}

		PanelGuide.SetActive(false);

		callback();
	}

	public void Begin(int moveCount, int burnCount)
	{
		playPanel.SetActive(true);
		timeText.gameObject.SetActive(false);
		SetMoveCount(moveCount);
		SetBurnCount(burnCount);
	}

	public void SetMoveCount(int count)
	{
		moveText.text = "x" + count.ToString();
	}

	public void SetBurnCount(int count)
	{
		burnButton.SetActive(count > 0);
		burnText.text = count.ToString();
	}

	public void SetTime(string text)
	{
		timeText.gameObject.SetActive(true);
		timeText.text = text;
	}

    public void ShowResult(bool isWin)
    {
        StartCoroutine(LazyResult(isWin));
        Debug.Log(isWin);
    }

    IEnumerator LazyResult(bool isWin)
    {
        yield return new WaitForSeconds(2f);
        ResultPanel.gameObject.SetActive(true);
        if (isWin)
        {
            ImageLose.SetActive(false);
            ImageWin.SetActive(true);

        }
        else
        {
            ImageLose.SetActive(true);
            ImageWin.SetActive(false);
        }
    }

    public void OnResultClick()
    {
        SceneManager.Instance.State = SceneManager.GameState.MAIN_MENU;
        GameManager.Instance.Playable = true;
    }

    public void OnRetryClick()
    {
        GameManager.Instance.Reset();
        ResultPanel.gameObject.SetActive(false);
    }

	public void ShowHelp()
	{
		HelpPanel.SetActive(true);
		GameManager.Instance.PauseGame();
	}

	public void HideHelp()
	{
		HelpPanel.SetActive(false);
		GameManager.Instance.ResumeGame();
	}
}
