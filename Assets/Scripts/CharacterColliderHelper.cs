﻿using UnityEngine;
using System.Collections;

public class CharacterColliderHelper : MonoBehaviour {

    public Character Character;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("Gate"))
        {
            GameManager.Instance.ShowResult(true);
        }
        else
        {
            GameManager.Instance.ShowResult(false);
        }
    }
}
