﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
    public InteractionReceiver Interation;
    public Character Character;
    public GameObject CameraContainer;
    public Background Background;
    public IngameScene IngameGUI;
    public List<Monster> MonsterList = new List<Monster>();

	public Button burnButton;
	public Text burnText;
	public Text moveText;

    public bool Playable = false;
	public Light2D litLOS;
	public Light2D viewLOS;
    public bool isBurning;
    public bool isDelay;
    public bool isBurningOver = true;
	private float originLitRadius;
    public float maxLitRadius = 100f;
    private float delayStep;
    private float burnStep;
    public float burnSpeed = 1.0f;
    public float burnDelay = 0.4f;

	public LevelInfo levelInfo;

	private int burnCount = 0;
	private int moveCount = 0;
	private float remainTime = -1f;

	public static string s_levelPath = "";

	List<GameObject> blockerList = new List<GameObject>();

    float cameraDistance;
    float maxDistance;

    public GameObject burnEffectPrefabs;

	public GameObject monster11Prefab;
	public GameObject monster12Prefab;

	void Start ()
	{
		monster11Prefab = Resources.Load("Monsters/Monster11") as GameObject;
		monster12Prefab = Resources.Load("Monsters/Monster12") as GameObject;

		LoadLevel();

		isBurningOver = true;
        originLitRadius = litLOS.LightRadius;
        cameraDistance = CameraContainer.transform.localPosition.y - Character.transform.localPosition.y;
        maxDistance = Background.TileRowCount * 10 - Camera.main.orthographicSize - 5;
	
		if (levelInfo == null)
		{
			levelInfo = new LevelInfo(){MaxBurn =999, MaxMove = 999, MaxTime = 10};
		}

		IngameGUI.ShowHint(this.OnShowHintFinish);
	}

	void OnShowHintFinish()
	{
		Playable = true;
		IngameGUI.Begin(levelInfo.MaxMove, levelInfo.MaxBurn);
	}

	void LoadLevel()
	{
		if (!string.IsNullOrEmpty(s_levelPath))
		{
			GameObject prefab = Resources.Load("Levels/" + s_levelPath) as GameObject;
			GameObject go = Instantiate(prefab);
			go.name = s_levelPath.Replace("/", "_");
			
       		Background = go.transform.FindChild("Background").GetComponent<Background>();

			levelInfo = (LevelInfo)Resources.Load("LevelInfos/" + s_levelPath);

			Transform monsters = go.transform.FindChild("Monsters");
			foreach(Transform m in monsters.transform)
			{
				if (string.Equals(m.name, "Monster11", System.StringComparison.OrdinalIgnoreCase))
				{
					CreateMonster(m, monster11Prefab);

				}
				else if (string.Equals(m.name, "Monster12", System.StringComparison.OrdinalIgnoreCase))
				{
					CreateMonster(m, monster12Prefab);
				}
			}
        }

		blockerList = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Blocker"));

		Interation.Load();
	}

	GameObject CreateMonster(Transform parent, GameObject prefab)
	{
		GameObject go = Instantiate(prefab);
		go.transform.parent = parent;
		go.transform.localPosition = Vector3.zero;
		return go;
	}

	void Update ()
	{
        Vector3 camPos = new Vector3(0, Character.transform.localPosition.y + cameraDistance, 0);
        if (camPos.y > maxDistance)
            camPos.y = maxDistance;
        CameraContainer.transform.localPosition = camPos;

        if (Playable)
        {
            if (isDelay)
            {
                delayStep += Time.deltaTime;
                if (delayStep > burnDelay)
                {
                    delayStep = 0;
                    isDelay = false;
                    isBurning = true;
                }
            }
            if (isBurning)
            {
                burnStep += burnSpeed * Time.deltaTime;
                isBurning = (burnStep < 1);
            }
            else
            {
                burnStep -= burnSpeed * Time.deltaTime;
            }
            burnStep = Mathf.Clamp01(burnStep);

		    litLOS.LightRadius = originLitRadius + Mathf.Lerp(0, maxLitRadius - originLitRadius, burnStep);
		    if (litLOS.GetComponent<FlickingController>() != null)
		    {
			    litLOS.LightRadius += Random.Range(-.1f, .1f);
		    }
            if (!isDelay && burnStep == 0 && !isBurningOver)
		    {
			    isBurningOver = true;
			    viewLOS.enabled = true;
			    foreach (GameObject item in blockerList) {
				    if (item.GetComponent<Collider>() != null)
				    {
					    item.GetComponent<Collider>().enabled = true;
				    }
				    if (item.GetComponent<Collider2D>() != null)
				    {
					    item.GetComponent<Collider2D>().enabled = true;
				    }
			    }

			    if (burnCount == levelInfo.MaxBurn)
			    {
				    StartCountDown();
			    }
		    }

		    UpdateUI();
        }
	}

    public void ActivateSkill()
    {
        if (!isBurning && isBurningOver)
        {
            isDelay = true;
            isBurningOver = false;
            viewLOS.enabled = false;

            Instantiate(burnEffectPrefabs, litLOS.transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));

            foreach (GameObject item in blockerList)
            {
                if (item.GetComponent<Collider>() != null)
                {
                    item.GetComponent<Collider>().enabled = false;
                }
                if (item.GetComponent<Collider2D>() != null)
                {
                    item.GetComponent<Collider2D>().enabled = false;
                }
            }
            burnCount++;
        }
    }

    public void Reset()
    {
        //viewLOS.enabled = true;
        //litLOS.LightRadius = originLitRadius;
        //Color newColor = Color.black;
        //newColor.a = 0;
        //Camera.main.backgroundColor = newColor;
        //Character.Reset();
        //foreach (Monster monst in MonsterList)
        //{
        //    monst.ResumePatrol();
        //}
        //remainTime = -1;
        //moveCount = 0;
        //burnCount = 0;
        //Playable = true;

        SceneManager.Instance.State = SceneManager.GameState.INGAME;
    }

    public void RevealAll(){
        viewLOS.enabled = false;
        litLOS.LightRadius = maxLitRadius;
        Color newColor = Color.white;
        newColor.a = 0;
        Camera.main.backgroundColor = newColor;
    }

	public void StartMoveCharacter(List<Vector3> path)
	{
		if (path.Count > 1)
		{
			moveCount++;
			Character.StartMove(path);
		}
	}

	public bool CanMove()
	{
		return (Playable &&  (moveCount < levelInfo.MaxMove));
	}

	void UpdateUI()
	{
		IngameGUI.SetMoveCount(levelInfo.MaxMove - moveCount);
		IngameGUI.SetBurnCount(levelInfo.MaxBurn - burnCount);

        if ((moveCount >= levelInfo.MaxMove) && Character.IsMoving == false && Playable == true)
        {
            ShowResult(false);
        }

		if (remainTime >= 0)
		{
			remainTime -= Time.deltaTime;
			remainTime = Mathf.Max(remainTime, 0);
			
			string time = string.Format("{0:00}", (int)remainTime % 60);
			IngameGUI.SetTime(time);
			
			if (remainTime == 0)
			{
				ShowResult(false);
			}
		}
	}

	void StartCountDown()
	{
		remainTime = levelInfo.MaxTime;
	}

	public void PauseGame()
	{
		Playable = false;
		Interation.EnableInteraction(false);
	}

	public void ResumeGame()
	{
		Playable = true;
		Interation.EnableInteraction(true);
	}
	
    public void Stop() { }

    public void ShowResult(bool isWin)
    {
        Playable = false;
        Character.StopMove();
        RevealAll();
        IngameGUI.ShowResult(isWin);
        foreach (Monster monst in MonsterList)
        {
            monst.PausePatrol();
        }
    }
}
