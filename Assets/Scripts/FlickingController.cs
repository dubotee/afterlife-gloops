﻿using UnityEngine;
using System.Collections;

public class FlickingController : MonoBehaviour {
    public bool isAppliedToAngle = false;
    public bool isAppliedToAlpha = false;
    private float minAlphaValue = 0.15f;
    private float maxAlphaValue = 0.25f;
    private float alphaLerpSpeed = 5.5f;

    private Color defaultRGB = Color.white;

    private float previousAlpha;
    private float nextAlpha;
    private float currAlphaStep = 0f;

    private float originRadius;
    private float originAngle;
	// Use this for initialization
	void Start () {
        //defaultRGB = this.GetComponent<DynamicLight>().lightMaterial.GetColor("_Color");

        previousAlpha = minAlphaValue;
        nextAlpha = Random.Range(minAlphaValue, maxAlphaValue);

        originRadius = GetComponent<Light2D>().LightRadius;
        originAngle = GetComponent<Light2D>().LightConeAngle;
	}
	
	// Update is called once per frame
	void Update () {
        if (isAppliedToAlpha)
        {
            if (currAlphaStep > 1)
            {
                previousAlpha = nextAlpha;
                nextAlpha = Random.Range(minAlphaValue, maxAlphaValue);
                currAlphaStep = 0.0f;
            }
            else
            {
                currAlphaStep += alphaLerpSpeed * Time.deltaTime;
                defaultRGB.a = Mathf.Lerp(previousAlpha, nextAlpha, currAlphaStep);
            }
            //this.GetComponent<DynamicLight>().lightMaterial.SetColor("_Color", defaultRGB);
        }
		if (this.gameObject != GameManager.Instance.litLOS.gameObject)
        	GetComponent<Light2D>().LightRadius = originRadius + Random.Range(-.1f, .1f);

        if (isAppliedToAngle) GetComponent<Light2D>().LightConeAngle = originAngle + Random.Range(-.1f, .1f);
	}
}
