﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Character will follow the path which user draw
/// When collide monster or wall, Character will die
/// </summary>
public class Character : MonoBehaviour {
    public float Speed;
    Vector3[] arrayPath;
    int currentWaypoint = 0;
    public bool IsMoving;

    public Light2D losView;
    Vector3 originalPos;
    CharacterColliderHelper colliderHelper;
	// Use this for initialization
	void Start () {
        originalPos = transform.localPosition;
        //colliderHelper = GetComponent<CharacterColliderHelper>();
        //colliderHelper.Character = this;
	}
	
	// Update is called once per frame
	void Update () {
        if (arrayPath != null && (currentWaypoint < arrayPath.Length - 1))
        {
            Vector3 dir = arrayPath[currentWaypoint] - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            losView.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
	}

    public void Reset()
    {
        transform.localPosition = originalPos;
    }

    public void StartMove(List<Vector3> path){
        currentWaypoint = 0;
        arrayPath = path.ToArray();
        MoveToWayPoint();
        IsMoving = true;
    }

    public void StopMove()
    {
        arrayPath = new Vector3[0];
        if(IsMoving)
            iTween.Pause(gameObject);
    }

    void MoveToWayPoint()
    {
        if (arrayPath.Length > 0)
        {
            float travelTime = Vector3.Distance(transform.position, arrayPath[currentWaypoint]) / Speed;
            iTween.MoveTo(gameObject,
                iTween.Hash("name", "Character",
                            "position", arrayPath[currentWaypoint],
                            "time", travelTime,
                            "easetype", iTween.EaseType.linear,
                            "oncomplete", "MoveToWayPoint",
                            "oncompletetarget", gameObject));
        
            currentWaypoint++;
            if (currentWaypoint == arrayPath.Length)
            {
                OnComplete();
            }
        }
    }

    void OnComplete()
    {
        iTween.Pause(gameObject);
        GameManager.Instance.Interation.ClearPath();
        IsMoving = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Gate"))
        {
            GameManager.Instance.ShowResult(true);
        }
        else
        {
            GameManager.Instance.ShowResult(false);
        }
    }
}
