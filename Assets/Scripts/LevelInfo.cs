﻿using UnityEngine;
using System.Collections;

public class LevelInfo : ScriptableObject
{
	public int MaxMove;
	public int MaxBurn;
	public int MaxTime;
}
