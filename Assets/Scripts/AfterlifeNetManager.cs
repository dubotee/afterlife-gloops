﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class AfterlifeNetManager : NetworkManager {
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        Vector3 pos = Vector3.zero;
        pos.y = -16f;

        GameObject player = (GameObject)Instantiate(base.playerPrefab, pos, Quaternion.Euler(0,0,90));
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }
}
