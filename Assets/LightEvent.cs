﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightEvent : MonoBehaviour 
{
    public Material hitLightMaterial;

    int id = 0;
    bool isDetected = false;
    Color c = Color.black;

    void Start()
    {
        id = gameObject.GetInstanceID();

        Light2D.RegisterEventListener(LightEventListenerType.OnStay, OnLightStay);
        Light2D.RegisterEventListener(LightEventListenerType.OnEnter, OnLightEnter);
        Light2D.RegisterEventListener(LightEventListenerType.OnExit, OnLightExit);
    }

    void OnDestroy()
    {
        /* (!) Make sure you unregister your events on destroy. If you do not
         * you might get strange errors (!) */

        Light2D.UnregisterEventListener(LightEventListenerType.OnStay, OnLightStay);
        Light2D.UnregisterEventListener(LightEventListenerType.OnEnter, OnLightEnter);
        Light2D.UnregisterEventListener(LightEventListenerType.OnExit, OnLightExit);
    }

    void Update()
    {
        if (isDetected)
            GetComponent<Renderer>().material.color = Color.Lerp(GetComponent<Renderer>().material.color, c, Time.deltaTime * 10f);
        else
            GetComponent<Renderer>().material.color = Color.Lerp(GetComponent<Renderer>().material.color, Color.black, Time.deltaTime * 5f);

        if (GameManager.Instance.isBurning)
        {
            GetComponent<Renderer>().material.color = Color.white;
        }
        isDetected = false;
    }

    void OnLightEnter(Light2D l, GameObject g)
    {
        //Debug.Log("Enter" + l.gameObject.name);
        if (g.GetInstanceID() == id)
        {
            c += l.LightColor;
        }
    }

    void OnLightStay(Light2D l, GameObject g)
    {
        if (g.GetInstanceID() == id)
        {
            isDetected = true;
        }
    }

    void OnLightExit(Light2D l, GameObject g)
    {
        if (g.GetInstanceID() == id)
        {
            c -= l.LightColor;
        }
    }
}
